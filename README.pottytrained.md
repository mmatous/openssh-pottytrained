# Pottytrained OpenSSH

OpenSSH that doesn't crap in your $HOME.

## General info

There're 2 commits, only 1 of which is of note. The second is this readme while
first consists of the actual changes.

It's not nice or elegant, it's pretty much cobbling it onto upstream
with a hammer, but it's meant to keep changes localized and as small
as possible. Minimal diff will ease maintenance and readability.

Bulk of changes is in `pathnames.h` and `ssh-copy-id`, the rest is mostly just string substitution oneliners along with docs and comments.

All new memory allocations are made in `pathnames.h` and saved in static vars. Again, this is to prevent modifications from being all over the place.

.ssh in the docs/comments now refers either to $HOME/.ssh or $XDG_*_DIR/ssh depending on your setup.

Drop-in for client. If `~/.ssh` exists, everything should work like before, if it doesn't exist OpenSSH will use XDG_* directories.

List of SW that may need attention after switching to pottytrained OpenSSH:
- KeepassXC entries, if you use it as ssh agent
- LUKS remote unlock tools/hooks, e.g. https://github.com/gsauthof/dracut-sshd, that copy keys into initramfs. It's recommended that root/.ssh stays as is.
- Dropbear needs to have keyfile directive modified
- Fail2ban if you start using sshd from different .service or binary
- Gitea requires moving authorized_keys from its user's $HOME/.ssh to .config/ssh and changing SSH_ROOT_PATH accordingly

Stuff that's confirmed to just work
- create and accept connections via password or public keys
- rsync uses ssh so it uses whatever locations ssh does

## Some caveats

Server is problematic because it doesn't know environment of user that's trying to log in. Possible workarounds could include reading common startup and env scripts, hardcoding `~/.local/share/ssh/config` or exec-ing shell as a user and printing `XDG_DATA_HOME` (but with little guarantees that env would be populated).

In the end, these patches use opt. 2, it makes for simplest changes and ssh exec-ing something sounds absolutely horifying.

Because reasons XDG_ dirs must be under `$HOME`. No crazy setups, sorry.

If neither ssh dir is found on remote, `ssh-copy-id` will create xdg dir even if remote ssh isn't pottytrained. (PRs solving this welcome, possible solution could be modifying `ssh -V` output on the remote and deciding accordingly).

If your server still only checks .ssh, please make sure that your `sshd_config` didn't come with AuthorizedKeysFile directive uncommented.

## FAQ

- Why not just hardcode most common xdg dirs and call it a day?
I started with "hardcoding bad" mindset. I had to capitulate with the sshd part though.

- Can I run this on server?
In theory. I do, on my low stakes, unimportant homeserver. But do you really need clean home on your server? I imagine risks aren't worth it. If you really want to try running it on servers, please create contingencies like executing a script that will copy all required files back to ~/.ssh and spin up unpatched sshd on pottytrained sshd's failure
