/* $OpenBSD: pathnames.h,v 1.31 2019/11/12 19:33:08 markus Exp $ */

/*
 * Author: Tatu Ylonen <ylo@cs.hut.fi>
 * Copyright (c) 1995 Tatu Ylonen <ylo@cs.hut.fi>, Espoo, Finland
 *                    All rights reserved
 *
 * As far as I am concerned, the code I have written for this software
 * can be used freely for any purpose.  Any derived versions of this
 * software must be clearly marked as such, and if the derived work is
 * incompatible with the protocol description in the RFC file, it must be
 * called by a name other than "ssh" or "Secure Shell".
 */

#define ETCDIR				"/etc"

#ifndef SSHDIR
#define SSHDIR				ETCDIR "/ssh"
#endif

#ifndef _PATH_SSH_PIDDIR
#define _PATH_SSH_PIDDIR		"/var/run"
#endif

/*
 * System-wide file containing host keys of known hosts.  This file should be
 * world-readable.
 */
#define _PATH_SSH_SYSTEM_HOSTFILE	SSHDIR "/ssh_known_hosts"
/* backward compat for protocol 2 */
#define _PATH_SSH_SYSTEM_HOSTFILE2	SSHDIR "/ssh_known_hosts2"

/*
 * Of these, ssh_host_key must be readable only by root, whereas ssh_config
 * should be world-readable.
 */
#define _PATH_SERVER_CONFIG_FILE	SSHDIR "/sshd_config"
#define _PATH_HOST_CONFIG_FILE		SSHDIR "/ssh_config"
#define _PATH_HOST_DSA_KEY_FILE		SSHDIR "/ssh_host_dsa_key"
#define _PATH_HOST_ECDSA_KEY_FILE	SSHDIR "/ssh_host_ecdsa_key"
#define _PATH_HOST_ED25519_KEY_FILE	SSHDIR "/ssh_host_ed25519_key"
#define _PATH_HOST_XMSS_KEY_FILE	SSHDIR "/ssh_host_xmss_key"
#define _PATH_HOST_RSA_KEY_FILE		SSHDIR "/ssh_host_rsa_key"
#define _PATH_DH_MODULI			SSHDIR "/moduli"

#ifndef _PATH_SSH_PROGRAM
#define _PATH_SSH_PROGRAM		"/usr/bin/ssh"
#endif

/*
 * The process id of the daemon listening for connections is saved here to
 * make it easier to kill the correct daemon when necessary.
 */
#define _PATH_SSH_DAEMON_PID_FILE	_PATH_SSH_PIDDIR "/sshd.pid"

/*
 * hardcoded macros from unpatched openssh now call ssh_str to
 * get correct path at runtime, ssh_str classifies whether file
 * is config, data or state and tries to retrieve correct dir
 * via intermediate functions that do nothing except pass
 * correct envvar & type to the actual logic
 *
 * *_tilde versions behave the same except they slap ~/ at the beginning
 * to minimize diff elsewhere in the code
*/

static char* pairasprintf(const char* prefix, const char* suffix) {
	int res_size = snprintf(NULL, 0, "%s%s", prefix, suffix) + 1;
	char* res = (char*) malloc(res_size);
	snprintf(res, res_size, "%s%s", prefix, suffix);
	return res;
}

typedef enum SSH_DIR_TYPE {
	SSH_DIR_CONFIG,
	SSH_DIR_DATA,
	SSH_DIR_STATE,
	SSH_DIR_N_TYPES,
} ssh_dir_type_t;

static char* get_ssh_dir(const char* envvar, ssh_dir_type_t type) {
	static char* ssh_dir[SSH_DIR_N_TYPES] = {0};
	const char* home_dir = getpwuid(getuid())->pw_dir;
	size_t home_len = strlen(home_dir) + 1; // + 1 == trailing '/'
	// str for type already figured out
	if (ssh_dir[type]) {
		return ssh_dir[type] + home_len;
	}

	static char* dot_dir = "/.ssh";
	ssh_dir[type] = pairasprintf(home_dir, dot_dir);
	struct stat st;
	int res = stat(ssh_dir[type], &st);
	// try legacy dir first for backwards compatibility
	if (!res && S_ISDIR(st.st_mode)) {
		return ssh_dir[type] + home_len;
	}

	const char* xdg_dir = getenv(envvar);
	// no xdg set, use legacy dir
	if (!xdg_dir) {
		return ssh_dir[type] + home_len;
	}
	// xdg set, use it, free legacy location
	free(ssh_dir[type]);

	// make xdg/ssh new fallback or user already migrated
	ssh_dir[type] = pairasprintf(xdg_dir, "/ssh");
	// return offset to get relative path
	char* rel_path = ssh_dir[type] + home_len;
	return rel_path;
}

__attribute__((unused))
static char* ssh_dir_config() {
	return get_ssh_dir("XDG_CONFIG_HOME", SSH_DIR_CONFIG);
}

__attribute__((unused))
static char* ssh_dir_data() {
	return get_ssh_dir("XDG_DATA_HOME", SSH_DIR_DATA);
}

__attribute__((unused))
static char* ssh_dir_state() {
	return get_ssh_dir("XDG_STATE_HOME", SSH_DIR_STATE);
}

typedef enum SSH_STR {
	_PATH_SSH_USER_DIR_S,
	_PATH_SSH_USER_STATE_DIR_TILDE_S,
	_PATH_SSH_USER_HOSTFILE_S,
	_PATH_SSH_USER_HOSTFILE2_S,
	_PATH_SSH_CLIENT_ID_DSA_S,
	_PATH_SSH_CLIENT_ID_ECDSA_S,
	_PATH_SSH_CLIENT_ID_RSA_S,
	_PATH_SSH_CLIENT_ID_ED25519_S,
	_PATH_SSH_CLIENT_ID_XMSS_S,
	_PATH_SSH_CLIENT_ID_ECDSA_SK_S,
	_PATH_SSH_CLIENT_ID_ED25519_SK_S,
	_PATH_SSH_USER_CONFFILE_S,
	_PATH_SSH_USER_PERMITTED_KEYS_S,
	_PATH_SSH_USER_PERMITTED_KEYS2_S,
	_PATH_SSH_USER_RC_S,
	SSH_STR_N_STRINGS
} ssh_str_t;

__attribute__((unused))
static char* ssh_str(ssh_str_t id, const char* suffix) {

	static char* strs[SSH_STR_N_STRINGS] = {0};
	if (!strs[id]) {
		const char* ssh_dir = NULL;
		switch (id) {
		case _PATH_SSH_USER_DIR_S: __attribute__((fallthrough));
		case _PATH_SSH_USER_CONFFILE_S: __attribute__((fallthrough));
		case _PATH_SSH_USER_RC_S:
			ssh_dir = ssh_dir_config();
			break;
		case _PATH_SSH_USER_HOSTFILE_S: __attribute__((fallthrough));
		case _PATH_SSH_USER_HOSTFILE2_S: __attribute__((fallthrough));
		case _PATH_SSH_USER_STATE_DIR_TILDE_S:
			ssh_dir = ssh_dir_state();
			break;
		default:
			ssh_dir = ssh_dir_data();
		}
		strs[id] = pairasprintf(ssh_dir, suffix);
	}
	return strs[id];
}

__attribute__((unused))
static char* ssh_str_tilde(ssh_str_t id, const char* suffix) {
	static char* tilde_strs[SSH_STR_N_STRINGS] = {0};
	if (!tilde_strs[id]) {
		tilde_strs[id] = pairasprintf("~/", ssh_str(id, suffix));
	}
	return tilde_strs[id];
}

/*
 * The directory in user's home directory in which the files reside. The
 * directory should be world-readable (though not all files are).
 */
#define _PATH_SSH_USER_DIR		(ssh_str(_PATH_SSH_USER_DIR_S, ""))
#define _PATH_SSH_USER_DIR_TILDE		(ssh_str_tilde(_PATH_SSH_USER_DIR_S, ""))
#define _PATH_SSH_USER_STATE_DIR_TILDE		(ssh_str_tilde(_PATH_SSH_USER_STATE_DIR_TILDE_S, ""))

// server doesn't know user's envvars :(
#define _PATH_SSH_USER_CONFIG_HARDCODED	".config/ssh"

/*
 * Per-user file containing host keys of known hosts.  This file need not be
 * readable by anyone except the user him/herself, though this does not
 * contain anything particularly secret.
 */
#define _PATH_SSH_USER_HOSTFILE		(ssh_str_tilde(_PATH_SSH_USER_HOSTFILE_S, "/known_hosts"))
/* backward compat for protocol 2 */
#define _PATH_SSH_USER_HOSTFILE2	(ssh_str_tilde(_PATH_SSH_USER_HOSTFILE2_S, "/known_hosts2"))

/*
 * Name of the default file containing client-side authentication key. This
 * file should only be readable by the user him/herself.
 */
#define _PATH_SSH_CLIENT_ID_DSA		(ssh_str(_PATH_SSH_CLIENT_ID_DSA_S, "/id_dsa"))
#define _PATH_SSH_CLIENT_ID_ECDSA	(ssh_str(_PATH_SSH_CLIENT_ID_ECDSA_S, "/id_ecdsa"))
#define _PATH_SSH_CLIENT_ID_RSA		(ssh_str(_PATH_SSH_CLIENT_ID_RSA_S, "/id_rsa"))
#define _PATH_SSH_CLIENT_ID_ED25519	(ssh_str(_PATH_SSH_CLIENT_ID_ED25519_S, "/id_ed25519"))
#define _PATH_SSH_CLIENT_ID_XMSS	(ssh_str(_PATH_SSH_CLIENT_ID_XMSS_S, "/id_xmss"))
#define _PATH_SSH_CLIENT_ID_ECDSA_SK	(ssh_str(_PATH_SSH_CLIENT_ID_ECDSA_SK_S, "/id_ecdsa_sk"))
#define _PATH_SSH_CLIENT_ID_ED25519_SK	(ssh_str(_PATH_SSH_CLIENT_ID_ED25519_SK_S, "/id_ed25519_sk"))

/*
 * Configuration file in user's home directory.  This file need not be
 * readable by anyone but the user him/herself, but does not contain anything
 * particularly secret.  If the user's home directory resides on an NFS
 * volume where root is mapped to nobody, this may need to be world-readable.
 */
#define _PATH_SSH_USER_CONFFILE		(ssh_str(_PATH_SSH_USER_CONFFILE_S, "/config"))

/*
 * File containing a list of those rsa keys that permit logging in as this
 * user.  This file need not be readable by anyone but the user him/herself,
 * but does not contain anything particularly secret.  If the user's home
 * directory resides on an NFS volume where root is mapped to nobody, this
 * may need to be world-readable.  (This file is read by the daemon which is
 * running as root.)
 */
#define _PATH_SSH_USER_PERMITTED_KEYS	_PATH_SSH_USER_CONFIG_HARDCODED "/authorized_keys"

/* backward compat for protocol v2 */
#define _PATH_SSH_USER_PERMITTED_KEYS2	_PATH_SSH_USER_CONFIG_HARDCODED "/authorized_keys2"

/*
 * Per-user and system-wide ssh "rc" files.  These files are executed with
 * /bin/sh before starting the shell or command if they exist.  They will be
 * passed "proto cookie" as arguments if X11 forwarding with spoofing is in
 * use.  xauth will be run if neither of these exists.
 */
#define _PATH_SSH_USER_RC		_PATH_SSH_USER_CONFIG_HARDCODED "/rc"
#define _PATH_SSH_SYSTEM_RC		SSHDIR "/sshrc"

/*
 * Ssh-only version of /etc/hosts.equiv.  Additionally, the daemon may use
 * ~/.rhosts and /etc/hosts.equiv if rhosts authentication is enabled.
 */
#define _PATH_SSH_HOSTS_EQUIV		SSHDIR "/shosts.equiv"
#define _PATH_RHOSTS_EQUIV		"/etc/hosts.equiv"

/*
 * Default location of askpass
 */
#ifndef _PATH_SSH_ASKPASS_DEFAULT
#define _PATH_SSH_ASKPASS_DEFAULT	"/usr/X11R6/bin/ssh-askpass"
#endif

/* Location of ssh-keysign for hostbased authentication */
#ifndef _PATH_SSH_KEY_SIGN
#define _PATH_SSH_KEY_SIGN		"/usr/libexec/ssh-keysign"
#endif

/* Location of ssh-pkcs11-helper to support keys in tokens */
#ifndef _PATH_SSH_PKCS11_HELPER
#define _PATH_SSH_PKCS11_HELPER		"/usr/libexec/ssh-pkcs11-helper"
#endif

/* Location of ssh-sk-helper to support keys in security keys */
#ifndef _PATH_SSH_SK_HELPER
#define _PATH_SSH_SK_HELPER		"/usr/libexec/ssh-sk-helper"
#endif

/* xauth for X11 forwarding */
#ifndef _PATH_XAUTH
#define _PATH_XAUTH			"/usr/X11R6/bin/xauth"
#endif

/* UNIX domain socket for X11 server; displaynum will replace %u */
#ifndef _PATH_UNIX_X
#define _PATH_UNIX_X "/tmp/.X11-unix/X%u"
#endif

/* for scp */
#ifndef _PATH_CP
#define _PATH_CP			"cp"
#endif

/* for sftp */
#ifndef _PATH_SFTP_SERVER
#define _PATH_SFTP_SERVER		"/usr/libexec/sftp-server"
#endif

/* chroot directory for unprivileged user when UsePrivilegeSeparation=yes */
#ifndef _PATH_PRIVSEP_CHROOT_DIR
#define _PATH_PRIVSEP_CHROOT_DIR	"/var/empty"
#endif

/* for passwd change */
#ifndef _PATH_PASSWD_PROG
#define _PATH_PASSWD_PROG             "/usr/bin/passwd"
#endif

#ifndef _PATH_LS
#define _PATH_LS			"ls"
#endif

/* Askpass program define */
#ifndef ASKPASS_PROGRAM
#define ASKPASS_PROGRAM         "/usr/lib/ssh/ssh-askpass"
#endif /* ASKPASS_PROGRAM */
